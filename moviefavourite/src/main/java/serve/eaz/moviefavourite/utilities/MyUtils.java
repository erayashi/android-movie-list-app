package serve.eaz.moviefavourite.utilities;

import android.content.res.Resources;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class MyUtils {

    MyUtils() {
    }

    public static String convertReleaseDate(Date releaseDate){
        if(releaseDate != null) {
            DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy", getLocale());
            return df.format(releaseDate);
        }

        return "-";
    }

    public static Locale getLocale(){
        return Resources.getSystem().getConfiguration().locale;
    }

    public static String getLanguage(){
        return Objects.equals(getLocale().getLanguage(), "in") ? "id" : "en";
    }
}
