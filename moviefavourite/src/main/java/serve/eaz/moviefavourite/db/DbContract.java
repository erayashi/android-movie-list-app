package serve.eaz.moviefavourite.db;

import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

public class DbContract {
    private static String TBL_FAV = "tbl_fav";

    public static final class FavColumns implements BaseColumns {
        public static String MOVIE_ID = "movie_id";
        public static String TITLE = "title";
        public static String VOTE_AVERAGE = "vote_average";
        public static String OVERVIEW = "overview";
        public static String OVERVIEW_IN = "overview_in";
        public static String GENRES = "genres";
        public static String GENRES_IN = "genres_in";
        public static String POSTER_PATH = "poster_path";
        public static String RELEASE_DATE = "release_date";
        public static String DIRECTED_BY = "directed_by";
        public static String STARING = "staring";
    }

    private static final String AUTHORITY = "com.serve.eaz.moviecatalogueuiux";

    public static final Uri CONTENT_URI = new Uri.Builder().scheme("content")
            .authority(AUTHORITY)
            .appendPath(TBL_FAV)
            .build();

    public static String getColumnString(Cursor cursor, String columnName) {
        return cursor.getString( cursor.getColumnIndex(columnName) );
    }

    public static int getColumnInt(Cursor cursor, String columnName) {
        return cursor.getInt( cursor.getColumnIndex(columnName) );
    }
}

