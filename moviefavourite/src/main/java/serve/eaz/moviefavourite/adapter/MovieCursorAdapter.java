package serve.eaz.moviefavourite.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;

import butterknife.ButterKnife;
import serve.eaz.moviefavourite.BuildConfig;
import serve.eaz.moviefavourite.R;
import serve.eaz.moviefavourite.models.Movie;

import static serve.eaz.moviefavourite.utilities.MyUtils.convertReleaseDate;

public class MovieCursorAdapter extends CursorAdapter {

    public MovieCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.item_movie_fav, viewGroup, false);
    }

    @Override
    public Cursor getCursor() {
        return super.getCursor();
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        if(cursor != null){
            ImageView ivPoster = view.findViewById(R.id.iv_poster);
            TextView tvTitle = view.findViewById(R.id.tv_title);
            TextView tvOverview = view.findViewById(R.id.tv_overview);
            TextView tvReleaseDate = view.findViewById(R.id.tv_release_date);
            ButterKnife.bind(this, view);
            Movie movie = new Movie(cursor);

            String posterUrl = movie.getPoster_path() != null ? String.format(BuildConfig.IMGURL, "w154", movie.getPoster_path()) : null;

            RequestOptions rq = new RequestOptions()
                    .placeholder(R.color.colorPrimary)
                    .fallback(R.drawable.ic_image_black_24dp)
                    .error(R.drawable.ic_image_black_24dp)
                    .centerCrop();

            Glide.with(context)
                    .load(posterUrl)
                    .transition(DrawableTransitionOptions.withCrossFade(200))
                    .apply(rq)
                    .into(ivPoster);

            tvTitle.setText(movie.getTitle());
            tvOverview.setText(movie.getOverview());
            tvReleaseDate.setText(convertReleaseDate(movie.getRelease_date()));
        }
    }
}
