package com.serve.eaz.moviecatalogueuiux.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.serve.eaz.moviecatalogueuiux.MovieListFragment;
import com.serve.eaz.moviecatalogueuiux.R;

public class MainTabsAdapter extends FragmentStatePagerAdapter  {

    private Context mContext;

    public MainTabsAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        MovieListFragment movieListFragment = new MovieListFragment();
        Bundle fBundle = new Bundle();
        switch (position){
            case 0:
                fBundle.putInt(MovieListFragment.ARG_TYPE, R.id.show_now_playing);
                movieListFragment.setArguments(fBundle);
                break;
            case 1:
                fBundle.putInt(MovieListFragment.ARG_TYPE, R.id.show_upcoming);
                movieListFragment.setArguments(fBundle);
                break;
            case 2:
                fBundle.putInt(MovieListFragment.ARG_TYPE, R.id.show_favourite_movie);
                movieListFragment.setArguments(fBundle);
                break;
            default:
                return null;
        }
        return movieListFragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return mContext.getResources().getString(R.string.now_playing);
            case 1:
                return mContext.getResources().getString(R.string.upcoming_movie);
            case 2:
                return mContext.getResources().getString(R.string.favourite);
            default:
                return null;
        }
    }



    @Override
    public int getCount() {
        return 3;
    }

    public Fragment getFragment(ViewPager container, int position, FragmentManager fm) {
        String name = makeFragmentName(container.getId(), position);
        return fm.findFragmentByTag(name);
    }

    private String makeFragmentName(int viewId, int index) {
        return "android:switcher:" + viewId + ":" + index;
    }
}
