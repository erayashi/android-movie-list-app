package com.serve.eaz.moviecatalogueuiux;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.serve.eaz.moviecatalogueuiux.adapters.CardMovieAdapter;
import com.serve.eaz.moviecatalogueuiux.models.ApiResponse;
import com.serve.eaz.moviecatalogueuiux.models.Movie;
import com.serve.eaz.moviecatalogueuiux.models.SearchResponse;
import com.serve.eaz.moviecatalogueuiux.presenters.MovieListInterface;
import com.serve.eaz.moviecatalogueuiux.presenters.MovieListPresenter;
import com.serve.eaz.moviecatalogueuiux.utilities.ItemClickSupport;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.ContentValues.TAG;

public class MovieListFragment extends Fragment implements MovieListInterface.View, MovieListInterface.Activity {
    //ARG
    public static final String ARG_QUERY = "arg_query";
    public static final String ARG_TYPE = "arg_type";

    //paging
    private Context context;
    private LinearLayoutManager mLinearLayoutManager;
    private CardMovieAdapter cardMovieAdapter;
    private int pageType;
    private int page = 1;
    private int maxPage = 1;
    private boolean loadDataRun = false;
    private String querySearch;
    private MovieListPresenter presenter;
    private ArrayList<Movie> mMovies = new ArrayList<>();

    //bind view
    @BindView(R.id.rv_movie_list) RecyclerView rvMovieList;
    @BindView(R.id.pb_load_movie_data) ProgressBar pbLoadMovie;
    @BindView(R.id.error_layout) LinearLayout errLayout;
    @BindView(R.id.tv_load_error) TextView errDesc;
    @BindView(R.id.btn_reload) Button btnRefresh;
    @BindString(R.string.fav_empty) String favEmpty;

    public MovieListFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new MovieListPresenter(this);
        if (getActivity() != null) {
            ((MainActivity) getActivity()).setActivityListener(this);
        }
        if (getArguments() != null) {
            pageType = getArguments().getInt(ARG_TYPE);
            querySearch = getArguments().getString(ARG_QUERY);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDataView(pageType, 1, querySearch);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_movie_list, container, false);
        ButterKnife.bind(this, v);
        mLinearLayoutManager = new LinearLayoutManager(context);
        cardMovieAdapter = new CardMovieAdapter(context);
        if(mMovies != null) this.mMovies.clear();
        rvMovieList.setId(pageType);
        rvMovieList.setHasFixedSize(true);
        rvMovieList.setLayoutManager(mLinearLayoutManager);
        rvMovieList.addOnScrollListener(rvScrollListener);
        rvMovieList.setAdapter(cardMovieAdapter);
        ItemClickSupport.addTo(rvMovieList).setOnItemClickListener(rvItemClickListener);

        btnRefresh.setOnClickListener(btnRefreshClick);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(pageType == R.id.show_favourite_movie) {
            getDataView(pageType, page, querySearch);
            Log.d(TAG, "onResume: yes");
        }
    }

    @Override
    public void onMovieListLoad(ApiResponse apiResponse, SearchResponse searchResponse) {
        ArrayList<Movie> loadMovies = new ArrayList<>();
        int totalResults = 0;
        String movFound;

        if(apiResponse != null){
            this.page = apiResponse.getPage();
            this.maxPage = apiResponse.getTotal_pages();
            totalResults = apiResponse.getTotal_results();
            loadMovies = apiResponse.getResults();
        }

        if (searchResponse != null){
            this.page = searchResponse.getPage();
            this.maxPage = searchResponse.getTotal_pages();
            totalResults = searchResponse.getTotal_results();
            loadMovies = searchResponse.getResults();
        }

        movFound = context.getResources().getQuantityString(R.plurals.movie_found, totalResults, totalResults);

        if(loadMovies.size() > 0){
            if(this.mMovies.size() == 0 || this.page == 1) {
                this.mMovies = loadMovies;
                Toast.makeText(context, movFound, Toast.LENGTH_SHORT).show();
            } else this.mMovies.addAll(loadMovies);

            cardMovieAdapter.setMovies(this.mMovies);
            page++;
        } else if(this.page == 1 && pageType == R.id.show_search_movie) {
            mMovies.clear();
            cardMovieAdapter.setMovies(this.mMovies);
            showError(movFound);
        }

        hideLoading();
    }

    View.OnClickListener btnRefreshClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            getDataView(pageType, page, querySearch);
        }
    };

    RecyclerView.OnScrollListener rvScrollListener = new RecyclerView.OnScrollListener() {
        private int currentVisibleItemCount;
        private int currentFirstVisibleItem;
        private int totalItem;

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            this.isScrollCompleted();
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            this.currentVisibleItemCount = rvMovieList.getChildCount();
            this.totalItem = mLinearLayoutManager.getItemCount();
            this.currentFirstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
        }

        private void isScrollCompleted() {
            if (totalItem - currentFirstVisibleItem <= currentVisibleItemCount
                    && !loadDataRun && pageType != R.id.show_favourite_movie) {

                if (page <= maxPage) {
                    Log.d(TAG, "isScrollCompleted: page " + page);
                    getDataView(pageType, page, querySearch);
                }
            }
        }
    };

    ItemClickSupport.OnItemClickListener rvItemClickListener = (recyclerView, position, v) -> {
        ImageView posterHolder = v.findViewById(R.id.iv_cm_poster);
        String transitionName = ViewCompat.getTransitionName(posterHolder);

        Intent movieDetailActivityIntent = new Intent(getActivity(), MovieDetailActivity.class);
        movieDetailActivityIntent.putExtra(MovieDetailActivity.EXTRA_ID, transitionName);
        movieDetailActivityIntent.putExtra(MovieDetailActivity.EXTRA_MOVIE_DATA, mMovies.get(position));

        assert getActivity()!=null;
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), posterHolder, transitionName);
        startActivity(movieDetailActivityIntent, options.toBundle());
    };

    private void getDataView(int pageId, int pageNo, @Nullable String query){
        switch (pageId){
            case R.id.show_upcoming:
                presenter.getUpcoming(pageNo);
                break;
            case R.id.show_search_movie:
                presenter.searchMovie(query, pageNo);
                break;
            case R.id.show_favourite_movie:
                assert getActivity()!= null;
                presenter.getFavourite(getActivity().getContentResolver());
                break;
            default:
                presenter.getNowPlaying(pageNo);
        }

        Log.d(TAG, "onCreate: " + query + ", Page: " + pageNo);
    }

    @Override
    public void onStartLoadMoview() {
        showLoading();
        hideError();
    }

    @Override
    public void movieLoadError(String err) {
        Toast.makeText(context, err, Toast.LENGTH_LONG).show();
        hideLoading();
        showError(err);
    }

    @Override
    public void onMovieFavLoad(ArrayList<Movie> movies) {
        this.mMovies = movies;
        cardMovieAdapter.setMovies(this.mMovies);

        if(movies.size() <= 0) {
            showError(favEmpty);
            rvMovieList.setVisibility(View.GONE);
        }
        hideLoading();
    }

    @Override
    public void searchMovieFromActivity(String query) {
        querySearch = query;
        presenter.searchMovie(querySearch, 1);
        Log.d(TAG, "searchMovieFromActivity: form listener");
    }

    public RecyclerView getRvMovies(){
        return rvMovieList;
    }

    private void showLoading(){
        loadDataRun = true;
        if(pbLoadMovie!=null) pbLoadMovie.setVisibility(View.VISIBLE);
    }

    private void hideLoading(){
        loadDataRun = false;
        if(pbLoadMovie!=null) pbLoadMovie.setVisibility(View.GONE);
    }

    private void showError(String e){
        if(mMovies.size() == 0) {
            errLayout.setVisibility(View.VISIBLE);
            errDesc.setText(e);
        }
    }

    private void hideError(){
        if(errLayout != null) if(errLayout.getVisibility() == View.VISIBLE) {
            errLayout.setVisibility(View.GONE);
            rvMovieList.setVisibility(View.VISIBLE);
        }
    }
}
