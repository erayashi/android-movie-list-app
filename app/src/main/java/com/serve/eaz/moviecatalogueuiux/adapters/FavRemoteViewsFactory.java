package com.serve.eaz.moviecatalogueuiux.adapters;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.serve.eaz.moviecatalogueuiux.BuildConfig;
import com.serve.eaz.moviecatalogueuiux.R;
import com.serve.eaz.moviecatalogueuiux.db.FavHelper;
import com.serve.eaz.moviecatalogueuiux.FavWidget;
import com.serve.eaz.moviecatalogueuiux.models.Movie;
import com.serve.eaz.moviecatalogueuiux.utilities.GlideRequestOption;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static com.serve.eaz.moviecatalogueuiux.utilities.MyUtils.convertReleaseDate;

public class FavRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
    private ArrayList<Movie> mMovies = new ArrayList<>();
    private FavHelper mFavHelper;
    private Context mContext;
    private int mAppWidgetId;

    public FavRemoteViewsFactory(Context mContext, Intent intent) {
        this.mContext = mContext;
        mFavHelper = new FavHelper(mContext);
        mAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
    }

    private void getFavMovie(){
        mFavHelper.open();
        mMovies = mFavHelper.queryAll();
        mFavHelper.close();
    }

    @Override
    public void onCreate() {
        getFavMovie();
    }

    @Override
    public void onDataSetChanged() {
        getFavMovie();
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        return mMovies.size();
    }

    @Override
    public RemoteViews getViewAt(int i) {
        if(getCount() > 0) {
            Movie movie = mMovies.get(i);
            RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.widget_item);

            rv.setTextViewText(R.id.tv_widget_rd, convertReleaseDate(movie.getRelease_date()));
            String posterUrl = movie.getPoster_path() != null ? String.format(BuildConfig.IMGURL, "w154", movie.getPoster_path()) : null;
            Bitmap bmp = null;
            try {
                bmp = Glide.with(mContext)
                        .asBitmap()
                        .load(posterUrl)
                        .apply(GlideRequestOption.widgetOption())
                        .into(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            Bundle extras = new Bundle();
            extras.putInt(FavWidget.EXTRA_ITEM, movie.getId());
            Intent fillInIntent = new Intent();
            fillInIntent.putExtras(extras);

            rv.setImageViewBitmap(R.id.iv_widget_poster, bmp);
            rv.setOnClickFillInIntent(R.id.imageView, fillInIntent);

            return rv;
        }
        return null;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
