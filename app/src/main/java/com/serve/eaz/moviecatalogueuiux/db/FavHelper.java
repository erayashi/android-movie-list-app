package com.serve.eaz.moviecatalogueuiux.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.serve.eaz.moviecatalogueuiux.models.Movie;

import java.util.ArrayList;

import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.MOVIE_ID;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.TBL_FAV;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns._ID;

public class FavHelper {

    private static String DATABASE_TABLE = TBL_FAV;
    private Context context;
    private DbHelper dbHelper;

    private SQLiteDatabase database;

    public FavHelper(Context context) {
        this.context = context;
    }

    public FavHelper open() throws SQLException {
        dbHelper = new DbHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        dbHelper.close();
    }

    public ArrayList<Movie> queryAll(){
        ArrayList<Movie> movies = new ArrayList<>();
        Cursor cursor = database.query(DATABASE_TABLE
                ,null
                ,null
                ,null
                ,null
                ,null
                ,_ID + " DESC");

        if (cursor != null) {
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    movies.add(new Movie(cursor));
                    cursor.moveToNext();
                } while (!cursor.isAfterLast());
            }
            cursor.close();
        }

        return movies;
    }

    public Cursor queryProvider(){
        return database.query(DATABASE_TABLE
                ,null
                ,null
                ,null
                ,null
                ,null
                ,_ID + " DESC");
    }

    public long insertProvider(ContentValues values){
        return database.insert(DATABASE_TABLE,null,values);
    }

    public int deleteByMovieIdProvider(String movieId){
        return database.delete(DATABASE_TABLE,MOVIE_ID + " = ?", new String[]{movieId});
    }

    public Cursor queryByMovieIdProvider(String MovieId){
        return database.query(DATABASE_TABLE,null
                ,MOVIE_ID + " = ?"
                ,new String[]{MovieId}
                ,null
                ,null
                ,null
                ,null);
    }

    public int updateProvider(String MovieId,ContentValues values){
        return database.update(DATABASE_TABLE,values,MOVIE_ID +" = ?",new String[]{MovieId} );
    }
}
