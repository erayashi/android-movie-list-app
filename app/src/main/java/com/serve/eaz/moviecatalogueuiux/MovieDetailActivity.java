package com.serve.eaz.moviecatalogueuiux;

import android.annotation.SuppressLint;
import android.appwidget.AppWidgetManager;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.serve.eaz.moviecatalogueuiux.adapters.CardRelatedAdapter;
import com.serve.eaz.moviecatalogueuiux.models.Credits;
import com.serve.eaz.moviecatalogueuiux.models.Movie;
import com.serve.eaz.moviecatalogueuiux.models.SearchResponse;
import com.serve.eaz.moviecatalogueuiux.presenters.MovieDetailInterface;
import com.serve.eaz.moviecatalogueuiux.presenters.MovieDetailPresenter;
import com.serve.eaz.moviecatalogueuiux.utilities.GlideRequestOption;
import com.serve.eaz.moviecatalogueuiux.utilities.ItemClickSupport;
import com.serve.eaz.moviecatalogueuiux.utilities.MyUtils;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.blurry.Blurry;

import static com.serve.eaz.moviecatalogueuiux.db.DbContract.CONTENT_URI;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.DIRECTED_BY;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.GENRES;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.GENRES_IN;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.MOVIE_ID;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.OVERVIEW;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.OVERVIEW_IN;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.POSTER_PATH;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.RELEASE_DATE;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.STARING;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.TITLE;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.VOTE_AVERAGE;
import static com.serve.eaz.moviecatalogueuiux.utilities.MyUtils.convertReleaseDate;
import static com.serve.eaz.moviecatalogueuiux.utilities.MyUtils.getGenres;
import static com.serve.eaz.moviecatalogueuiux.utilities.MyUtils.getLanguage;
import static com.serve.eaz.moviecatalogueuiux.utilities.MyUtils.originalStrReleaseDate;
import static com.serve.eaz.moviecatalogueuiux.utilities.MyUtils.revLangAPI;

public class MovieDetailActivity extends AppCompatActivity implements MovieDetailInterface.View{
    public static final String EXTRA_ID = "extra_id";
    public static final String EXTRA_MOVIE_DATA = "extra_movie_data";

    private Boolean fav = false;
    private Uri uri;
    private Movie mMovie;
    private Movie mMovieRev;

    @BindView(R.id.iv_dt_poster) ImageView ivDtPoster;
    @BindView(R.id.iv_dt_bg) ImageView ivDtBg;
    @BindView(R.id.detail_toolbar) Toolbar toolbar;
    @BindView(R.id.tv_dt_desc) TextView tvDtDesc;
    @BindView(R.id.tv_dt_rate) TextView tvDtRate;
    @BindView(R.id.tv_dt_release_date) TextView tvDtRd;
    @BindView(R.id.tv_dt_genres) TextView tvDtGenres;
    @BindView(R.id.tv_dt_starring) TextView tvDtStarring;
    @BindView(R.id.tv_dt_direct) TextView tvDtDirect;
    @BindView(R.id.rv_dt_related) RecyclerView rvDtRelated;
    @BindView(R.id.fab_fav) FloatingActionButton fabFav;
    @BindString(R.string.synopsis_hint) String synopsisHint;
    @BindString(R.string.share_format) String shareFormat;
    @BindString(R.string.share_title) String shareTitle;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        supportPostponeEnterTransition();
        ButterKnife.bind(this);

        fabFav.hide();

        Bundle extras = getIntent().getExtras();
        assert extras != null;
        mMovie = extras.getParcelable(EXTRA_MOVIE_DATA);

        toolbar.setTitle(mMovie != null ? mMovie.getTitle() : null);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        uri = Uri.parse(CONTENT_URI + "/" + mMovie.getId());
        MovieDetailPresenter mPresenter = new MovieDetailPresenter(this);

        if(uri != null){
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if(cursor != null){
                if(cursor.moveToFirst()) {
                    mMovie = new Movie(cursor);
                    fav = true;
                    fabFav.setImageResource(R.drawable.ic_favorite_24dp);
                    tvDtDirect.setText(mMovie.getDirected_by());
                    tvDtStarring.setText(mMovie.getStaring());
                    tvDtGenres.setText(mMovie.getGenreStr());
                }
                cursor.close();
            }
        }

        Log.d(MovieDetailActivity.class.getSimpleName(), "onCreate: lang = " + getLanguage());
        Log.d(MovieDetailActivity.class.getSimpleName(), "onCreate: lang_rev = " + revLangAPI());
        mPresenter.getMovieDetail(mMovie.getId());
        mPresenter.getMovieDetailWithLang(mMovie.getId(), revLangAPI());
        mPresenter.getMovieDetailRelated(mMovie.getId());
        if(!fav) mPresenter.getMovieDetailCredits(mMovie.getId());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String imageTransitionName = extras.getString(EXTRA_ID);
            if(imageTransitionName != null)
                ivDtPoster.setTransitionName(imageTransitionName);
        }

        loadImagePoster();
        loadTextView();

        fabFav.setOnClickListener(fabFavClick);
    }

    private View.OnClickListener fabFavClick = view -> {
        if(fav){
            removeFav();
        } else {
            insertFav();
        }

        Intent intent = new Intent(this, FavWidget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        sendBroadcast(intent);
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_movie_detail_toolbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share_view:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = String.format(shareFormat, mMovie.getTitle(), mMovie.getId());
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, mMovie.getTitle());
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, shareTitle));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMovieDetailSuccess(Movie movie) {
        mMovie = movie;
        mMovie.setGenreStr(getGenres(movie.getGenres()));
        loadTextView();
    }

    @Override
    public void onMovieDetailSuccessWithLang(Movie movie) {
        mMovieRev = movie;
        fabFav.show();
    }

    @Override
    public void onMovieDetailCreditsSuccess(Credits credits) {
        tvDtStarring.setText(MyUtils.getStarring(credits.getCast()));
        tvDtDirect.setText(MyUtils.getDirector(credits.getCrew()));
    }

    @Override
    public void onMovieDetailRelatedSuccess(SearchResponse searchResponse) {
        LinearLayoutManager relatedLinLayMan = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        CardRelatedAdapter cardRelatedAdapter = new CardRelatedAdapter(this);
        cardRelatedAdapter.setMovies(searchResponse.getResults());
        rvDtRelated.setHasFixedSize(true);
        rvDtRelated.setLayoutManager(relatedLinLayMan);
        rvDtRelated.setAdapter(cardRelatedAdapter);

        ItemClickSupport.addTo(rvDtRelated).setOnItemClickListener((recyclerView, position, v) -> {
            ImageView posterHolder = v.findViewById(R.id.iv_rt_pic);
            String transitionName = ViewCompat.getTransitionName(posterHolder);

            Intent movieDetailActivityIntent = new Intent(this, MovieDetailActivity.class);
            movieDetailActivityIntent.putExtra(MovieDetailActivity.EXTRA_ID, transitionName);
            movieDetailActivityIntent.putExtra(MovieDetailActivity.EXTRA_MOVIE_DATA, searchResponse.getResults().get(position));

            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, posterHolder, transitionName);

            startActivity(movieDetailActivityIntent, options.toBundle());
        });
    }



    @Override
    public void onMovieDetailError(Throwable e, boolean main) {
        e.printStackTrace();
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        if (main) finish();
    }

    private void loadImagePoster(){
        String posterUrl = mMovie.getPoster_path() != null ? String.format(BuildConfig.IMGURL, "w342", mMovie.getPoster_path()) : null;
        Glide.with(this)
                .asBitmap()
                .load(posterUrl)
                .apply(GlideRequestOption.poster())
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        supportStartPostponedEnterTransition();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        loadBlurryBg(resource);
                        supportStartPostponedEnterTransition();
                        return false;
                    }
                })
                .into(ivDtPoster);
    }

    private void loadBlurryBg(Bitmap bmp){
        Blurry.with(MovieDetailActivity.this)
                .sampling(2)
                .color(Color.argb(150, 0, 0, 0))
                .from(bmp)
                .into(ivDtBg);
    }

    private void loadTextView(){
        String synopsis = TextUtils.isEmpty(mMovie.getOverview()) ? synopsisHint : mMovie.getOverview();
        tvDtDesc.setText(synopsis);
        tvDtRate.setText(String.valueOf(mMovie.getVote_average()));
        tvDtRd.setText(convertReleaseDate(mMovie.getRelease_date()));
        tvDtGenres.setText(mMovie.getGenreStr());
    }

    private void insertFav(){
        ContentValues values = new ContentValues();
        values.put(MOVIE_ID, mMovie.getId());
        values.put(TITLE, mMovie.getTitle());
        values.put(VOTE_AVERAGE, mMovie.getVote_average());
        values.put(POSTER_PATH, mMovie.getPoster_path());
        values.put(RELEASE_DATE, originalStrReleaseDate(mMovie.getRelease_date()));
        values.put(DIRECTED_BY, String.valueOf(tvDtDirect.getText()));
        values.put(STARING, String.valueOf(tvDtStarring.getText()));
        values.put(OVERVIEW, mMovie.getOverview());
        values.put(OVERVIEW_IN, mMovieRev.getOverview());
        values.put(GENRES, getGenres(mMovie.getGenres()));
        values.put(GENRES_IN, getGenres(mMovieRev.getGenres()));

        Uri insertUri = getContentResolver().insert(CONTENT_URI, values);
        assert insertUri != null;
        if(Integer.parseInt(insertUri.getLastPathSegment()) > 0) fabFav.setImageResource(R.drawable.ic_favorite_24dp);
        fav = true;
    }

    private void removeFav(){
        getContentResolver().delete(uri, null, null);
        fabFav.setImageResource(R.drawable.ic_favorite_border_24dp);
        fav = false;
    }
}