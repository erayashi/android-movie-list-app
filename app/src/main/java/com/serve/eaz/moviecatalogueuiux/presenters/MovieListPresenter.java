package com.serve.eaz.moviecatalogueuiux.presenters;

import android.content.ContentResolver;
import android.database.Cursor;

import com.serve.eaz.moviecatalogueuiux.models.ApiResponse;
import com.serve.eaz.moviecatalogueuiux.models.Movie;
import com.serve.eaz.moviecatalogueuiux.models.SearchResponse;
import com.serve.eaz.moviecatalogueuiux.network.APICtrl;
import com.serve.eaz.moviecatalogueuiux.network.MovieIface;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.serve.eaz.moviecatalogueuiux.db.DbContract.CONTENT_URI;

public class MovieListPresenter implements MovieListInterface.Presenter {
    private MovieListInterface.View view;
    private MovieIface apiCtrl = new APICtrl().movieAPI();
    public MovieListPresenter(MovieListInterface.View view) {
        this.view = view;
    }

    @Override
    public void searchMovie(String query, int page) {
        view.onStartLoadMoview();
        apiCtrl.searchMovie(query, page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::searchResponse, this::errResponse);
    }

    @Override
    public void getNowPlaying(int page) {
        view.onStartLoadMoview();
        apiCtrl.getNowPlaying(page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::apiResponse, this::errResponse);
    }

    @Override
    public void getUpcoming(int page) {
        view.onStartLoadMoview();
        apiCtrl.getUpcoming(page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::apiResponse, this::errResponse);

    }

    @Override
    public void getFavourite(ContentResolver contentResolver) {
        view.onStartLoadMoview();
        loadFavourite(contentResolver)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::favResponse, this::errResponse);
    }

    private Observable<ArrayList<Movie>> loadFavourite(ContentResolver contentResolver){
        return Observable.create(e -> {
            try {
                ArrayList<Movie> movies = new ArrayList<>();
                Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);
                if (cursor != null) {
                    if(cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        do {
                            movies.add(new Movie(cursor));
                            cursor.moveToNext();
                        } while (!cursor.isAfterLast());
                    }
                    cursor.close();
                }
                e.onNext(movies);
            } catch (Exception exc){
                e.onError(exc);
            }
        });
    }

    private void searchResponse(SearchResponse searchResponse){
        view.onMovieListLoad(null, searchResponse);
    }

    private void apiResponse(ApiResponse apiResponse){
        view.onMovieListLoad(apiResponse, null);
    }

    private void favResponse(ArrayList<Movie> movies) {
        view.onMovieFavLoad(movies);
    }

    private void errResponse(Throwable error){
        view.movieLoadError(error.getMessage());
    }
}
