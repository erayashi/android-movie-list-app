package com.serve.eaz.moviecatalogueuiux.models;

import java.util.ArrayList;

public class Credits {
    private int id;
    private ArrayList<Cast> cast;
    private ArrayList<Crew> crew;

    public Credits(ArrayList<Cast> cast, ArrayList<Crew> crew) {
        this.cast = cast;
        this.crew = crew;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Credits.Cast> getCast() {
        return cast;
    }

    public ArrayList<Credits.Crew> getCrew() {
        return crew;
    }

    public class Cast {
        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class Crew {
        private int id;
        private String job;
        private String name;

        public Crew(int id, String job, String name) {
            this.id = id;
            this.job = job;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getJob() {
            return job;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
