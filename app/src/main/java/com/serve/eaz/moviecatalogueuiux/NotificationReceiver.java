package com.serve.eaz.moviecatalogueuiux;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;
import com.serve.eaz.moviecatalogueuiux.models.Movie;
import com.serve.eaz.moviecatalogueuiux.services.UpcomingService;
import com.serve.eaz.moviecatalogueuiux.utilities.DailyReminderPreference;

import java.util.Calendar;

public class NotificationReceiver extends BroadcastReceiver {
    private static final String TAG = NotificationReceiver.class.getSimpleName();
    public static final String DAILY_REMINDER = "daily_reminder";
    public static final String UPCOMING_REMINDER = "upcoming_reminder";
    public static final String EXTRA_UPCOMING_DATA = "extra_upcoming_data";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: " + intent.getAction());
        PendingIntent pendingIntentMain = null;
        String type = "";
        String title = "";
        String body = "";

        if(DAILY_REMINDER.equals(intent.getAction())){
            Intent intentMain = new Intent(context, MainActivity.class);
            pendingIntentMain = PendingIntent.getActivity(context, R.id.notif_daily_reminder, intentMain, 0);
            type = DAILY_REMINDER;
            title = getResourceString(context, R.string.notif_daily_title);
            body = getResourceString(context, R.string.notif_daily_body);
        } else if (UPCOMING_REMINDER.equals(intent.getAction())){
            Movie movie = intent.getParcelableExtra(EXTRA_UPCOMING_DATA);
            Intent intentUpcoming = new Intent(context, MovieDetailActivity.class);
            intentUpcoming.putExtra(MovieDetailActivity.EXTRA_MOVIE_DATA, movie);
            pendingIntentMain = PendingIntent.getActivity(context, R.id.notif_upcoming, intentUpcoming, 0);
            type = UPCOMING_REMINDER;
            title = movie.getTitle();
            body = String.format(getResourceString(context, R.string.upcoming_today), title);
        }

        NotificationCompat.Builder nfBuilder = new NotificationCompat.Builder(context, type)
                .setContentIntent(pendingIntentMain)
                .setSmallIcon(R.drawable.ic_main)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_main))
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true);

        NotificationManagerCompat nm = NotificationManagerCompat.from(context);
        nm.notify(R.id.notif_daily_reminder ,nfBuilder.build());
    }

    public void setDailyReminder(Context context){
        DailyReminderPreference DRPref = new DailyReminderPreference(context);

        Intent intent = new Intent(context, NotificationReceiver.class);
        intent.setAction(DAILY_REMINDER);

        boolean alarmUp = (PendingIntent.getBroadcast(context, R.id.notif_daily_reminder, intent, PendingIntent.FLAG_NO_CREATE) != null);

        if(!alarmUp) {
            Log.d(TAG, "setDailyReminder: create alarm");
            String time = DRPref.getPrefTimeStr();
            String timeArray[] = time.split(":");
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0]));
            calendar.set(Calendar.MINUTE, Integer.parseInt(timeArray[1]));
            calendar.set(Calendar.SECOND, 0);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            if (alarmManager!=null) {
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, R.id.notif_daily_reminder, intent, 0);
                alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
                Log.d(TAG, "setDailyReminder: create alarm success");
            }
        } else {
            Log.d(TAG, "setDailyReminder: alarm exist");
        }
    }

    public void setUpcomingReminder(Context context){
        GcmNetworkManager gcmNetworkManager = GcmNetworkManager.getInstance(context);

        Task periodicTask = new PeriodicTask.Builder()
                .setService(UpcomingService.class)
                .setPeriod(3600)
                .setFlex(20)
                .setTag(UpcomingService.TASK_UPCOMING)
                .setRequiredNetwork(Task.NETWORK_STATE_ANY)
                .setPersisted(true)
                .build();
        gcmNetworkManager.schedule(periodicTask);

        Log.d(TAG, "setUpcomingReminder: set periodic task");
    }

    private String getResourceString(Context context, int idRes){
        return context.getResources().getString(idRes);
    }
}
