package com.serve.eaz.moviecatalogueuiux.services;

import android.content.Context;
import android.content.Intent;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;
import com.serve.eaz.moviecatalogueuiux.NotificationReceiver;
import com.serve.eaz.moviecatalogueuiux.models.ApiResponse;
import com.serve.eaz.moviecatalogueuiux.models.Movie;
import com.serve.eaz.moviecatalogueuiux.network.APICtrl;

import java.util.Date;
import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.serve.eaz.moviecatalogueuiux.utilities.MyUtils.convertReleaseDate;

public class UpcomingService extends GcmTaskService {
    public static final String TASK_UPCOMING = "task_upcoming";
    private APICtrl apiCtrl = new APICtrl();

    @Override
    public int onRunTask(TaskParams taskParams) {
        apiCtrl.movieAPI()
                .getUpcoming(1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::response, this::err);
        return GcmNetworkManager.RESULT_SUCCESS;
    }

    private void err(Throwable throwable) {
        throwable.printStackTrace();
    }

    private void response(ApiResponse apiResponse) {
        for(Movie movie: apiResponse.getResults()){
            Date date = new Date();

            if(Objects.equals(convertReleaseDate(date), convertReleaseDate(movie.getRelease_date()))){
                Intent brIntent = new Intent(getApplicationContext(), NotificationReceiver.class);
                brIntent.setAction(NotificationReceiver.UPCOMING_REMINDER);
                brIntent.putExtra(NotificationReceiver.EXTRA_UPCOMING_DATA, movie);
                getApplicationContext().sendBroadcast(brIntent);
            }
        }
    }
}
