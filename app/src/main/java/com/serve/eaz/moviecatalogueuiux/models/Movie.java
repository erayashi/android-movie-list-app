package com.serve.eaz.moviecatalogueuiux.models;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.DIRECTED_BY;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.GENRES;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.GENRES_IN;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.MOVIE_ID;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.OVERVIEW;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.OVERVIEW_IN;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.POSTER_PATH;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.RELEASE_DATE;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.STARING;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.TITLE;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.VOTE_AVERAGE;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.getColumnInt;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.getColumnString;
import static com.serve.eaz.moviecatalogueuiux.utilities.MyUtils.getLanguage;
import static com.serve.eaz.moviecatalogueuiux.utilities.MyUtils.getLocale;

public class Movie implements Parcelable {
    private int id;
    private Double vote_average;
    private String title;
    private String poster_path;
    private String overview;
    private Date release_date;
    private ArrayList<Genre> genres;
    private String staring;
    private String directed_by;
    private String genreStr;

    public Movie(){

    }

    public Movie(Double vote_average, String title, String poster_path, String overview, Date release_date, ArrayList<Genre> genres) {
        this.vote_average = vote_average;
        this.title = title;
        this.poster_path = poster_path;
        this.overview = overview;
        this.release_date = release_date;
        this.genres = genres;
    }

    public Movie(Cursor cursor){
        this.id = getColumnInt(cursor, MOVIE_ID);
        this.title = getColumnString(cursor, TITLE);
        this.vote_average = Double.parseDouble(getColumnString(cursor, VOTE_AVERAGE));
        this.poster_path = getColumnString(cursor, POSTER_PATH);
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd", getLocale());
            this.release_date = df.parse(getColumnString(cursor, RELEASE_DATE));
        } catch (Exception e){
            this.release_date = null;
        }
        this.directed_by = getColumnString(cursor, DIRECTED_BY);
        this.staring = getColumnString(cursor, STARING);
        this.overview = getColumnString(cursor, (Objects.equals(getLanguage(), "id") ? OVERVIEW_IN : OVERVIEW));
        this.genreStr = getColumnString(cursor, (Objects.equals(getLanguage(), "id") ? GENRES_IN : GENRES));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getVote_average() {
        return vote_average;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public String getOverview() {
        return overview;
    }

    public Date getRelease_date() {
        return release_date;
    }

    public ArrayList<Genre> getGenres() {
        return genres;
    }

    public String getStaring() {
        return staring;
    }

    public String getDirected_by() {
        return directed_by;
    }

    public String getGenreStr() {
        return genreStr;
    }

    public void setGenreStr(String genreStr) {
        this.genreStr = genreStr;
    }

    public class Genre {
        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeValue(this.vote_average);
        dest.writeString(this.title);
        dest.writeString(this.poster_path);
        dest.writeString(this.overview);
        dest.writeLong(this.release_date != null ? this.release_date.getTime() : -1);
        dest.writeString(this.staring);
        dest.writeString(this.directed_by);
        dest.writeString(this.genreStr);
    }

    protected Movie(Parcel in) {
        this.id = in.readInt();
        this.vote_average = (Double) in.readValue(Double.class.getClassLoader());
        this.title = in.readString();
        this.poster_path = in.readString();
        this.overview = in.readString();
        long tmpRelease_date = in.readLong();
        this.release_date = tmpRelease_date == -1 ? null : new Date(tmpRelease_date);
        this.staring = in.readString();
        this.directed_by = in.readString();
        this.genreStr = in.readString();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
