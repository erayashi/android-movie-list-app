package com.serve.eaz.moviecatalogueuiux.presenters;

import com.serve.eaz.moviecatalogueuiux.models.Credits;
import com.serve.eaz.moviecatalogueuiux.models.Movie;
import com.serve.eaz.moviecatalogueuiux.models.SearchResponse;

public interface MovieDetailInterface {
    interface View {
        void onMovieDetailSuccess(Movie movie);
        void onMovieDetailSuccessWithLang(Movie movie);
        void onMovieDetailCreditsSuccess(Credits credits);
        void onMovieDetailRelatedSuccess(SearchResponse searchResponse);
        void onMovieDetailError(Throwable e, boolean main);
    }

    interface Presenter {
        void getMovieDetail(int movieId);
        void getMovieDetailWithLang(int movieId, String lang);
        void getMovieDetailCredits(int movieId);
        void getMovieDetailRelated(int movieId);
    }
}
