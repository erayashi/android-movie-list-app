package com.serve.eaz.moviecatalogueuiux.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.serve.eaz.moviecatalogueuiux.R;

import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.DIRECTED_BY;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.GENRES;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.GENRES_IN;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.MOVIE_ID;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.OVERVIEW;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.OVERVIEW_IN;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.POSTER_PATH;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.RELEASE_DATE;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.STARING;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.TITLE;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns.VOTE_AVERAGE;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.FavColumns._ID;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.TBL_FAV;

public class DbHelper extends SQLiteOpenHelper {
    private static String DATABASE_NAME = "db_movie_catalogue";

    private static final int DATABASE_VERSION = 2;

    private Context context;

    DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    private String createTblFav(String TBL){
        String sqlFormat = context.getResources().getString(R.string.sql_create_table_fav);
        return String.format(sqlFormat, TBL,
                _ID, MOVIE_ID, TITLE, VOTE_AVERAGE,
                POSTER_PATH, RELEASE_DATE, DIRECTED_BY, STARING,
                OVERVIEW, OVERVIEW_IN, GENRES, GENRES_IN);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createTblFav(TBL_FAV));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TBL_FAV);
        onCreate(db);
    }
}
