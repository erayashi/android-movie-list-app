package com.serve.eaz.moviecatalogueuiux;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.serve.eaz.moviecatalogueuiux.models.Movie;
import com.serve.eaz.moviecatalogueuiux.network.APICtrl;
import com.serve.eaz.moviecatalogueuiux.utilities.DailyReminderPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DevActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.btn_shouc_notif) Button btnShowUcNotif;
    @BindView(R.id.btn_show_dr_notif) Button btnShowDrNotif;
    @BindView(R.id.tv_time) TextView tvTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dev);
        ButterKnife.bind(this);

        DailyReminderPreference dailyReminderPreference = new DailyReminderPreference(this);

        tvTime.setText(dailyReminderPreference.getPrefTimeStr());

        btnShowDrNotif.setOnClickListener(this);
        btnShowUcNotif.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_show_dr_notif:
                Intent intent = new Intent(this, NotificationReceiver.class);
                intent.setAction(NotificationReceiver.DAILY_REMINDER);
                sendBroadcast(intent);
                break;
            case R.id.btn_shouc_notif:
                APICtrl apiCtrl = new APICtrl();
                apiCtrl.movieAPI().getMovieDetail(284052)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Observer<Movie>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(Movie movie) {
                                Intent intentUc = new Intent(getApplicationContext(), NotificationReceiver.class);
                                intentUc.setAction(NotificationReceiver.UPCOMING_REMINDER);
                                intentUc.putExtra(NotificationReceiver.EXTRA_UPCOMING_DATA, movie);
                                sendBroadcast(intentUc);
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {

                            }
                        });
                break;
        }
    }
}
