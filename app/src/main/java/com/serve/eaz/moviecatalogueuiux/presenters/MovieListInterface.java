package com.serve.eaz.moviecatalogueuiux.presenters;

import android.content.ContentResolver;
import android.database.Cursor;

import com.serve.eaz.moviecatalogueuiux.models.ApiResponse;
import com.serve.eaz.moviecatalogueuiux.models.Movie;
import com.serve.eaz.moviecatalogueuiux.models.SearchResponse;

import java.util.ArrayList;

import io.reactivex.annotations.Nullable;

public interface MovieListInterface {
    interface View {
        void onMovieListLoad(@Nullable ApiResponse apiResponse, @Nullable SearchResponse searchResponse);
        void onStartLoadMoview();
        void movieLoadError(String err);
        void onMovieFavLoad(ArrayList<Movie> movies);
    }

    interface Presenter {
        void searchMovie(@Nullable String query, int Page);
        void getNowPlaying(int Page);
        void getUpcoming(int Page);
        void getFavourite(ContentResolver contentResolver);
    }

    interface Activity {
        void searchMovieFromActivity(String query);
    }
}
