package com.serve.eaz.moviecatalogueuiux.network;

import android.annotation.SuppressLint;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.serve.eaz.moviecatalogueuiux.BuildConfig;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.serve.eaz.moviecatalogueuiux.utilities.MyUtils.getLanguage;

public class APICtrl {

    private Retrofit retrofit;

    public APICtrl() {
        retrofit = rfBuilder(getLanguage()).build();
    }

    public APICtrl(String lang) {
        retrofit = rfBuilder(lang).build();
    }

    private Retrofit.Builder rfBuilder(String lang){
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASEURL)
                .client(okHttpClientSet(lang))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(myGson()));
    }

    private static Gson myGson(){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            @SuppressLint("SimpleDateFormat") DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            @Override
            public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                    throws JsonParseException {
                try {
                    return df.parse(json.getAsString());
                } catch (ParseException e) {
                    return null;
                }
            }
        }).serializeNulls();

        return gsonBuilder.create();
    }

    private OkHttpClient okHttpClientSet(String lang){

        return new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(chain -> {
                    final String langs = (lang == null ? getLanguage() : lang);
                    Request ori = chain.request();
                    HttpUrl oriUrl = ori.url();
                    HttpUrl newUrl = oriUrl.newBuilder()
                            .addQueryParameter("api_key", BuildConfig.API_KEY)
                            .addQueryParameter("language", langs)
                            .build();

                    Request.Builder requestBuilder = ori.newBuilder().url(newUrl);

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }).build();
    }

    public MovieIface movieAPI(){
        return retrofit.create(MovieIface.class);
    }

}
