package com.serve.eaz.moviecatalogueuiux.adapters;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.serve.eaz.moviecatalogueuiux.BuildConfig;
import com.serve.eaz.moviecatalogueuiux.R;
import com.serve.eaz.moviecatalogueuiux.models.Movie;
import com.serve.eaz.moviecatalogueuiux.utilities.GlideRequestOption;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CardRelatedAdapter extends RecyclerView.Adapter<CardRelatedAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Movie> movies;

    public CardRelatedAdapter(Context context) {
        this.context = context;
    }

    public void setMovies(ArrayList<Movie> movies){
        this.movies = movies;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.rv_related_thumbnail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Movie movie = movies.get(position);
        holder.tvRtName.setText(movie.getTitle());

        ViewCompat.setTransitionName(holder.ivRtPic, String.valueOf(movie.getId()));

        String picUrl = movie.getPoster_path() != null ? String.format(BuildConfig.IMGURL,"w154",movie.getPoster_path()) : null;

        Glide.with(context)
                .load(picUrl)
                .transition(DrawableTransitionOptions.withCrossFade(200))
                .apply(GlideRequestOption.cardOption())
                .into(holder.ivRtPic);
    }

    @Override
    public int getItemCount() {
        if(this.movies == null) return 0;
        return this.movies.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_rt_name) TextView tvRtName;
        @BindView(R.id.iv_rt_pic) ImageView ivRtPic;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
