package com.serve.eaz.moviecatalogueuiux.presenters;

import com.serve.eaz.moviecatalogueuiux.models.Credits;
import com.serve.eaz.moviecatalogueuiux.models.Movie;
import com.serve.eaz.moviecatalogueuiux.models.SearchResponse;
import com.serve.eaz.moviecatalogueuiux.network.APICtrl;
import com.serve.eaz.moviecatalogueuiux.network.MovieIface;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MovieDetailPresenter implements MovieDetailInterface.Presenter{

    private MovieDetailInterface.View view;
    private MovieIface apiCtrl = new APICtrl().movieAPI();
    private boolean main = false;

    public MovieDetailPresenter(MovieDetailInterface.View view) {
        this.view = view;
    }

    @Override
    public void getMovieDetail(int movieId) {
        main = true;
        apiCtrl.getMovieDetail(movieId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::apiResponse, this::errResponse);
    }

    @Override
    public void getMovieDetailWithLang(int movieId, String lang) {
        new APICtrl(lang).movieAPI()
                .getMovieDetail(movieId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::apiResponseWithLang, this::errResponse);
    }

    private void apiResponseWithLang(Movie movie) {
        view.onMovieDetailSuccessWithLang(movie);
    }

    @Override
    public void getMovieDetailCredits(int movieId) {
        main = false;
        apiCtrl.getMovieDetailCredits(movieId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::creditsResponse, this::errResponse);
    }

    @Override
    public void getMovieDetailRelated(int movieId) {
        main = false;
        apiCtrl.getMovieDetailRelated(movieId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::relatedResponse, this::errResponse);
    }

    private void apiResponse(Movie movie) {
        view.onMovieDetailSuccess(movie);
    }

    private void creditsResponse(Credits credits) {
        view.onMovieDetailCreditsSuccess(credits);
    }

    private void relatedResponse(SearchResponse searchResponse){
        view.onMovieDetailRelatedSuccess(searchResponse);
    }

    private void errResponse(Throwable error){
        view.onMovieDetailError(error, main);
    }
}
