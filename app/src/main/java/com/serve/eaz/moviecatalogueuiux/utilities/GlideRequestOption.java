package com.serve.eaz.moviecatalogueuiux.utilities;

import com.bumptech.glide.request.RequestOptions;
import com.serve.eaz.moviecatalogueuiux.R;

public class GlideRequestOption {
    public static RequestOptions poster(){
        return new RequestOptions()
                .placeholder(R.color.colorDarkerGrey)
                .fallback(R.drawable.ic_broken_image_128dp)
                .error(R.drawable.ic_broken_image_128dp)
                .centerCrop()
                .dontAnimate();
    }

    public static RequestOptions cardOption(){
        return new RequestOptions()
                .placeholder(R.color.colorDarkerGrey)
                .fallback(R.drawable.ic_broken_image_128dp)
                .error(R.drawable.ic_broken_image_128dp)
                .skipMemoryCache(true)
                .centerCrop();
    }

    public static RequestOptions widgetOption(){
        return new RequestOptions()
                .placeholder(R.color.colorDarkerGrey)
                .fallback(R.drawable.ic_broken_image_128dp)
                .error(R.drawable.ic_broken_image_128dp)
                .fitCenter();
    }
}
