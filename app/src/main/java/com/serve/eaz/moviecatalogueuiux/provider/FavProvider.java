package com.serve.eaz.moviecatalogueuiux.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.serve.eaz.moviecatalogueuiux.db.FavHelper;

import static com.serve.eaz.moviecatalogueuiux.db.DbContract.AUTHORITY;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.CONTENT_URI;
import static com.serve.eaz.moviecatalogueuiux.db.DbContract.TBL_FAV;

public class FavProvider extends ContentProvider {

    private static final int FAV = 1;
    private static final int FAV_ID = 2;
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private FavHelper favHelper;

    static {

        // content://com.serve.eaz.moviecatalogueuiux/tbl_fav
        sUriMatcher.addURI(AUTHORITY, TBL_FAV, FAV);

        // content://com.serve.eaz.moviecatalogueuiux/tbl_fav/{id}
        sUriMatcher.addURI(AUTHORITY, TBL_FAV+ "/#", FAV_ID);
    }

    @Override
    public boolean onCreate() {
        favHelper = new FavHelper(getContext());
        favHelper.open();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        Cursor cursor = null;
        switch (sUriMatcher.match(uri)){
            case FAV:
                cursor = favHelper.queryProvider();
                break;
            case FAV_ID:
                cursor = favHelper.queryByMovieIdProvider(uri.getLastPathSegment());
                break;
        }

        if (cursor != null && getContext()!=null){
            cursor.setNotificationUri(getContext().getContentResolver(),uri);
        }

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        long added = 0;
        switch (sUriMatcher.match(uri)){
            case FAV:
                added = favHelper.insertProvider(contentValues);
                break;
        }

        if (added > 0 && getContext()!=null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return Uri.parse(CONTENT_URI + "/" + added);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        int deleted = 0;

        switch (sUriMatcher.match(uri)) {
            case FAV_ID:
                deleted =  favHelper.deleteByMovieIdProvider(uri.getLastPathSegment());
                break;
        }

        if (deleted > 0 && getContext()!=null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return deleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        int updated ;
        switch (sUriMatcher.match(uri)) {
            case FAV_ID:
                updated =  favHelper.updateProvider(uri.getLastPathSegment(),contentValues);
                break;
            default:
                updated = 0;
                break;
        }

        if (updated > 0 && getContext()!=null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return updated;
    }
}
