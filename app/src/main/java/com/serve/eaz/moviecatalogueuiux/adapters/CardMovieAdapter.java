package com.serve.eaz.moviecatalogueuiux.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.serve.eaz.moviecatalogueuiux.BuildConfig;
import com.serve.eaz.moviecatalogueuiux.R;
import com.serve.eaz.moviecatalogueuiux.models.Movie;
import com.serve.eaz.moviecatalogueuiux.utilities.GlideRequestOption;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CardMovieAdapter extends RecyclerView.Adapter<CardMovieAdapter.CardMovieViewHolder> {
    private Context context;
    private ArrayList<Movie> movies;

    public CardMovieAdapter(Context context) {
        this.context = context;
    }

    public void setMovies(ArrayList<Movie> pMovies){
        this.movies = pMovies;
        notifyDataSetChanged();
    }

    private ArrayList<Movie> getMovies() {
        return movies;
    }

    @Override
    public CardMovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.rv_card_movie, parent, false);
        return new CardMovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CardMovieViewHolder holder, int position) {
        Movie movie = getMovies().get(position);
        holder.tvTitle.setText(movie.getTitle());
        holder.tvRate.setText(String.valueOf(movie.getVote_average()));

        String synopsis = TextUtils.isEmpty(movie.getOverview()) ? holder.synopsisHint : movie.getOverview();
        holder.tvDesc.setText(synopsis);

        ViewCompat.setTransitionName(holder.ivPoster, String.valueOf(movie.getId()));

        try {
            Date releaseDate = movie.getRelease_date();
            if(releaseDate != null){
                @SuppressLint("SimpleDateFormat") DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy");
                holder.tvRd.setText(df.format(releaseDate));
            } else {
                holder.tvRd.setText("-");
            }

            String posterUrl = movie.getPoster_path() != null ? String.format(BuildConfig.IMGURL, "w154", movie.getPoster_path()) : null;

            Glide.with(context)
                    .load(posterUrl)
                    .transition(DrawableTransitionOptions.withCrossFade(200))
                    .apply(GlideRequestOption.cardOption())
                    .into(holder.ivPoster);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if(movies == null)
            return 0;
        return movies.size();
    }

    class CardMovieViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.iv_cm_poster) ImageView ivPoster;
        @BindView(R.id.tv_cm_title) TextView tvTitle;
        @BindView(R.id.tv_cm_rd) TextView tvRd;
        @BindView(R.id.tv_cm_desc) TextView tvDesc;
        @BindView(R.id.tv_cm_rate) TextView tvRate;
        @BindString(R.string.synopsis_hint) String synopsisHint;

        CardMovieViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
