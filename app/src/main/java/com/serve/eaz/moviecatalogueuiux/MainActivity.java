package com.serve.eaz.moviecatalogueuiux;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.serve.eaz.moviecatalogueuiux.adapters.MainTabsAdapter;
import com.serve.eaz.moviecatalogueuiux.presenters.MovieListInterface;
import com.serve.eaz.moviecatalogueuiux.utilities.DailyReminderPreference;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //view
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.tabLayout) TabLayout tabLayout;
    @BindView(R.id.tabPager) ViewPager viewPager;
    @BindView(R.id.search_fragment) LinearLayout searchFg;

    @BindString(R.string.search) String strSrch;

    protected TabLayout.Tab selTab;
    private MainTabsAdapter mainTabsAdapter;

    public MovieListInterface.Activity activityListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        mainTabsAdapter = new MainTabsAdapter(getSupportFragmentManager(), getApplicationContext());
        viewPager.setAdapter(mainTabsAdapter);
        viewPager.setOffscreenPageLimit(3);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(tabSelectedListener);

        DailyReminderPreference preference = new DailyReminderPreference(this);
        if (preference.getPrefFirstRun()) {
            NotificationReceiver notificationReceiver = new NotificationReceiver();
            notificationReceiver.setDailyReminder(getApplicationContext());
            notificationReceiver.setUpcomingReminder(getApplicationContext());
            preference.setPrefFirstRunBool(false);
        }
    }

    TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            navigationView.getMenu().getItem(tab.getPosition()).setChecked(true);
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            //tabunselected
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
            //tabreselected
            recylerViewToTop(tab.getPosition());
        }
    };

    private void recylerViewToTop(int tabPosition){
        FragmentManager fm = getSupportFragmentManager();
        Fragment f = mainTabsAdapter.getFragment(viewPager, tabPosition, fm);

        if(f != null){
            MovieListFragment fragment = (MovieListFragment) f;
            fragment.getRvMovies().smoothScrollToPosition(0);
            if(getSupportActionBar() != null) getSupportActionBar().show();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.search_view);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint(strSrch);

        final MovieListFragment movieListFragment = new MovieListFragment();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                if(fragmentManager.findFragmentByTag(MovieListFragment.class.getSimpleName()) == null) {
                    Bundle fBundle = new Bundle();
                    fBundle.putInt(MovieListFragment.ARG_TYPE, R.id.show_search_movie);
                    fBundle.putString(MovieListFragment.ARG_QUERY, query);
                    movieListFragment.setArguments(fBundle);

                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.search_fragment, movieListFragment, MovieListFragment.class.getSimpleName());
                    fragmentTransaction.commit();
                    searchFg.animate().alpha(1).setDuration(300);
                } else {
                    activityListener.searchMovieFromActivity(query);
                }
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                searchFg.setVisibility(View.VISIBLE);
                searchFg.animate().alpha((float) 0.8).setDuration(300);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                searchFg.animate().alpha(0).setDuration(300).withEndAction(() -> {
                    searchFg.setVisibility(View.GONE);
                    getSupportFragmentManager().beginTransaction().remove(movieListFragment).commit();
                });
                return true;
            }
        });

        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int SelItem = item.getItemId();
        if(SelItem == R.id.nav_set_lang){
            Intent iLang = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(iLang);
        } else if(SelItem == R.id.nav_dev_set) {
            Intent iDev = new Intent(this, DevActivity.class);
            startActivity(iDev);
        } else {
            switch (SelItem) {
                case R.id.nav_nowplaying:
                    selTab = tabLayout.getTabAt(0);
                    if (selTab!=null)
                        selTab.select();
                    break;
                case R.id.nav_upcoming:
                    selTab = tabLayout.getTabAt(1);
                    if(selTab!=null)
                        selTab.select();
                    break;
                case R.id.nav_favorite:
                    selTab = tabLayout.getTabAt(2);
                    if(selTab!=null)
                        selTab.select();
                    break;
            }
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setActivityListener(MovieListInterface.Activity activityListen){
        this.activityListener = activityListen;
    }

}
