package com.serve.eaz.moviecatalogueuiux.utilities;

import android.content.res.Resources;
import android.text.TextUtils;

import com.serve.eaz.moviecatalogueuiux.models.Credits;
import com.serve.eaz.moviecatalogueuiux.models.Movie;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class MyUtils {

    MyUtils() {
    }

    public static String convertReleaseDate(Date releaseDate){
        if(releaseDate != null) {
            DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy", getLocale());
            return df.format(releaseDate);
        }

        return "-";
    }

    public static String originalStrReleaseDate(Date releaseDate){
        if(releaseDate != null) {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd", getLocale());
            return df.format(releaseDate);
        }

        return "-";
    }

    public static String getGenres(ArrayList<Movie.Genre> genres){
        if(genres != null) {
            ArrayList<String> genreBuilder = new ArrayList<>();
            for (Movie.Genre genre: genres){
                genreBuilder.add(genre.getName());
            }
            return TextUtils.join(", ", genreBuilder);
        }
        return "-";
    }

    public static String getStarring(ArrayList<Credits.Cast> casts){
        if(casts != null && casts.size() > 0){
            int lim = casts.size() < 5 ? casts.size() : 5;
            ArrayList<String> castsBuilder = new ArrayList<>();
            for (int g = 0; g < lim; g++){
                castsBuilder.add(casts.get(g).getName());
            }
            return TextUtils.join(", ", castsBuilder);
        }
        return "-";
    }

    public static String getDirector(ArrayList<Credits.Crew> crews){
        String director = "-";
        if(crews != null && crews.size() > 0){
            for (Credits.Crew crew: crews){
                if(Objects.equals(crew.getJob(), "Director")) {
                    director = crew.getName();
                    break;
                }
            }
        }
        return director;
    }

    public static Locale getLocale(){
        return Resources.getSystem().getConfiguration().locale;
    }

    public static String getLanguage(){
        return Objects.equals(getLocale().getLanguage(), "in") ? "id" : "en";
    }

    public static String revLangAPI(){
        return Objects.equals(getLanguage(), "id") ? "en" : "id";
    }
}
