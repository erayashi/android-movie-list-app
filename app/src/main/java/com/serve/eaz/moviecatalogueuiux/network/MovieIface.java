package com.serve.eaz.moviecatalogueuiux.network;

import com.serve.eaz.moviecatalogueuiux.models.ApiResponse;
import com.serve.eaz.moviecatalogueuiux.models.Credits;
import com.serve.eaz.moviecatalogueuiux.models.Movie;
import com.serve.eaz.moviecatalogueuiux.models.SearchResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieIface {
    @GET("movie/now_playing?region=us")
    Observable<ApiResponse> getNowPlaying(@Query("page") int page);

    @GET("movie/upcoming?region=us")
    Observable<ApiResponse> getUpcoming(@Query("page") int page);

    @GET("search/movie")
    Observable<SearchResponse> searchMovie(@Query("query") String query, @Query("page") int page);

    @GET("movie/{movie_id}")
    Observable<Movie> getMovieDetail(@Path("movie_id") int movie_id);

    @GET("movie/{movie_id}/credits")
    Observable<Credits> getMovieDetailCredits(@Path("movie_id") int movie_id);

    @GET("movie/{movie_id}/recommendations")
    Observable<SearchResponse> getMovieDetailRelated(@Path("movie_id") int movie_id);
}
