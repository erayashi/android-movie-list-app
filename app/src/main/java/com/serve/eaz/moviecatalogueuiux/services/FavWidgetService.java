package com.serve.eaz.moviecatalogueuiux.services;

import android.content.Intent;
import android.widget.RemoteViewsService;

import com.serve.eaz.moviecatalogueuiux.adapters.FavRemoteViewsFactory;

public class FavWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new FavRemoteViewsFactory(this.getApplicationContext(), intent);
    }
}
