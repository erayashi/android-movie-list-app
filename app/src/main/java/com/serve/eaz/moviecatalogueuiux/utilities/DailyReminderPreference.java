package com.serve.eaz.moviecatalogueuiux.utilities;

import android.content.Context;
import android.content.SharedPreferences;

public class DailyReminderPreference {
    private SharedPreferences mSharedPreferences;
    private static final String PREF_TIME_STR = "pref_time_str";
    private static final String PREF_FIRST_RUN_BOOL = "pref_first_run_bool";

    public DailyReminderPreference(Context context) {
        String PREF_NAME = "DailyReminderPreference";
        mSharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public void setPrefTimeStr (String date){
        mSharedPreferences.edit().putString(PREF_TIME_STR, date).apply();
    }

    public String getPrefTimeStr(){
        return mSharedPreferences.getString(PREF_TIME_STR, "08:15");
    }

    public boolean getPrefFirstRun() {
        return mSharedPreferences.getBoolean(PREF_FIRST_RUN_BOOL, true);
    }

    public void setPrefFirstRunBool(boolean fr){
        mSharedPreferences.edit().putBoolean(PREF_FIRST_RUN_BOOL, fr).apply();
    }
}
